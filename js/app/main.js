define(
  ["jquery", "underscore", "app/map", "jqueryui"],
  function ($, _, Map) {
    "use strict";

    var highlight,
        searchResults;

    var infoTemplate = _.template($('#info-template').html());




    var displayFeatureInfo = function (pixel) {
      var feature = Map.getMap().forEachFeatureAtPixel(pixel, function (feature) {
        return feature;
      });

      $('.dialog .close-btn').trigger('click');

      if (feature) {

        var info = {};
        info.photo = feature.G.foto;
        info.photoBig = feature.G.foto2;

        info.names = [];
        if (feature.G.osoba_1 !== null) {
          info.names.push({
            name: feature.G.osoba_1,
            birth: feature.G.ur_1,
            death: feature.G.sm_1
          })
        }

        if (feature.G.osoba_2 !== null) {
          info.names.push({
            name: feature.G.osoba_2,
            birth: feature.G.ur_2,
            death: feature.G.sm_2
          })
        }

        if (feature.G.osoba_3 !== null) {
          info.names.push({
            name: feature.G.osoba_3,
            birth: feature.G.ur_3,
            death: feature.G.sm_3
          })
        }

        $(infoTemplate(info)).appendTo('body');

      }

      if (feature !== highlight) {
        if (feature) {
          Map.showFeature(feature);
        }
        highlight = feature;
      }
    };


    var displaySearchResults = function (searchData) {
      searchResults = [];

      if (searchData.search_name !== "") {
        _.each(Map.getObjects(), function (object) {
          var props = object.G,
              searchName = searchData.search_name.toLowerCase(),
              name1 = (props.osoba_1 === null) ? "" : props.osoba_1.toLowerCase(),
              name2 = (props.osoba_2 === null) ? "" : props.osoba_2.toLowerCase(),
              name3 = (props.osoba_3 === null) ? "" : props.osoba_3.toLowerCase();

          if (
            name1.indexOf(searchName) > -1
            || name2.indexOf(searchName) > -1
            || name3.indexOf(searchName) > -1
          ) {
            searchResults.push(object);
          }
        });

        // if (searchData.search_death !== "") {
        //   var filter = [];

        //   _.each(searchResults, function (object) {
        //     var props = object.G,
        //       searchName = searchData.search_name.toLowerCase(),
        //       name1 = (props.osoba_i === null) ? "" : props.osoba_1.toLowerCase(),
        //       name2 = (props.osoba_ii === null) ? "" : props.osoba_2.toLowerCase(),
        //       name3 = (props.osoba_iii === null) ? "" : props.osoba_3.toLowerCase();

        //     if (
        //       (name1.indexOf(searchName) > -1 && props.sm_1 == searchData.search_death)
        //       || (name2.indexOf(searchName) > -1 && props.sm_2 == searchData.search_death)
        //       || (name3.indexOf(searchName) > -1 && props.sm_3 == searchData.search_death)
        //     ) {
        //       filter.push(object);
        //     }
        //   });

        //   searchResults = filter;
        // }

      } else {
        _.each(objects, function (object) {
          var props = object.G;

          if (
            props.sm_1 == searchData.search_death
            || props.sm_2 == searchData.search_death
            || props.sm_3 == searchData.search_death
          ) {
            searchResults.push(object);
          }
        });
      }

      Map.showSearchResults(searchResults);





    };




    var getAvailableNames = function () {
      var names = [];

      _.each(Map.getObjects(), function (o) {
        names.push(o.G.osoba_1);
        names.push(o.G.osoba_2);
        names.push(o.G.osoba_3);
      });

      names = _.compact(names);
      names = _.uniq(names);
      names = _.sortBy(names, function (name) {
        return name;
      });

      return names;
    };






    // EVENTS

    $(document).on('hideLoader', function (event) {
      $("#loader").fadeOut(200);

    }).on('showLoader', function (event) {
      $("#loader").fadeIn(200);

    }).on('setAutocomplete', function (event) {
      $('#search_name').autocomplete({
        minLength: 3,
        source: getAvailableNames()
      });
    });


    $('body').on('click', '.dialog .close-btn', function (event) {
      if (highlight !== null) {
        Map.hideFeature(highlight);
        highlight = null;
      }

      $(this).closest('.dialog').fadeOut(200, function () {
        $(this).remove();
      });
    });


    $('#search-form').on('submit', function (event) {

      $(this).find('input').trigger('blur');

      var data = $(this).serializeArray(),
          searchData = {};
      _.each(data, function(object) {
        searchData[object.name] = object.value;
      });

      if (searchData.search_name === "" && searchData.search_death === "") {
        Map.showSearchResults([]);
        return false;
      }


      $(document).trigger('showLoader');

      try {
        displaySearchResults(searchData);
      }
      catch (err) {
        console.error(err);
      }

      $(document).trigger('hideLoader');

      return false;
    });


    Map.getMap().on('click', function (event) {
      displayFeatureInfo(event.pixel);
    });


});
