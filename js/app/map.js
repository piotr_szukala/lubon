  define(
  ["jquery", "underscore", "http://openlayers.org/en/v3.13.0/build/ol.js"],
  function ($, _, ol) {
    "use strict";

    var objects = null;

    var map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        }),
          new ol.layer.Tile({
            //extent: [-13884991, 2870341, -7455066, 6338219],
            source: new ol.source.TileWMS({
            url: 'http://pro-gis.pl:8080/geoserver/lubon/gwc/service/wms',
            params: {'LAYERS': 'orto-gotowe-clip', 'TILED': true},
            serverType: 'geoserver'
          })
        }),

         new ol.layer.Tile({
            //extent: [-13884991, 2870341, -7455066, 6338219],
            source: new ol.source.TileWMS({
            url: 'http://pro-gis.pl:8080/geoserver/lubon/gwc/service/wms',
            params: {'LAYERS': 'plan,drzewa,ogrodzenie,budynki', 'TILED': true},
            serverType: 'geoserver'
          })
        }),
        //  new ol.layer.Tile({
        //     //extent: [-13884991, 2870341, -7455066, 6338219],
        //     source: new ol.source.TileWMS({
        //     url: 'http://pro-gis.pl:8080/geoserver/lubon/gwc/service/wms',
        //     params: {'LAYERS': 'groby', 'TILED': true},
        //     serverType: 'geoserver'
        //   })
        // })


      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([16.849315,52.348061]),
        zoom: 17,
        maxZoom: 22
      })
    });


    var featureOverlay = new ol.layer.Vector({
      source: new ol.source.Vector(),
      map: map,
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#f00',
          width: 1
        }),
        fill: new ol.style.Fill({
          color: 'rgba(255,0,0,0.1)'
        })
      })
    });

    var searchOverlay = new ol.layer.Vector({
      source: new ol.source.Vector(),
      map: map,
      style: new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '#ff0',
          width: 1
        }),
        fill: new ol.style.Fill({
          color: 'rgb(255,255,0)'
        })
      })
    });




    var styles = {
      'MultiPolygon': new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: 'rgb(0, 0, 255)',
          width: 1
        }),
        fill: new ol.style.Fill({
          color: 'rgba(0, 0, 255, 0.1)'
        })
      })
    };

    var styleFunction = function (feature) {
      return styles[feature.getGeometry().getType()];
    };



    var processJSON = function (data) {
      objects = (new ol.format.GeoJSON()).readFeatures(data);

      var vectorSource = new ol.source.Vector({
        features: objects
      });

      var vectorLayer = new ol.layer.Vector({
        source: vectorSource,
        style: styleFunction
      });

      map.addLayer(vectorLayer);


      $(document).trigger('hideLoader');
      $(document).trigger('setAutocomplete');
    }



    $.ajax({
      url: "http://pro-gis.pl:8080/geoserver/lubon/wfs",

      data: {
        service: 'WFS',
        version: '1.1.0',
        request: 'GetFeature',
        typename: 'feature:groby',
        outputFormat: 'text/javascript',
				srsName: "EPSG:900913",
				geometryName: "geom"
      },
      method: "get",
      dataType: 'jsonp',
      jsonpCallback:'parseResponse',
      jsonp:'format_options'
    }).done(function (data) {
      processJSON(data);
    });


    return {
      getMap: function () {
        return map;
      },
      getObjects: function () {
        return objects;
      },

      showFeature: function (feature) {
        featureOverlay.getSource().addFeature(feature);
      },
      hideFeature: function (feature) {
        featureOverlay.getSource().removeFeature(feature);
      },

      showSearchResults: function (features) {
        searchOverlay.getSource().clear();

        _.each(features, function (feature) {
          searchOverlay.getSource().addFeature(feature);
        });

        // Przybliżanie do wyszukanego elementu
        var extent = searchOverlay.getSource().getExtent();
        map.getView().fit(extent, map.getSize());

      }
    };
});
