requirejs.config({
  paths: {
    jquery: "https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min",
    jqueryui: "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min",
    underscore: "https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min"
  },
  shim: {
    jqueryui: {
      deps: ["jquery"]
    }
  }
});


requirejs(['app/main']);